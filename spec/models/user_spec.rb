require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:father_name) }
  it { should validate_presence_of(:family_name) }
  it { should validate_presence_of(:birthday) }
  it { should validate_presence_of(:telephone) }
  it { should validate_presence_of(:personal_data_agreement) }
end
