FactoryGirl.define do
  factory :course do
    name "Test course"
    description "Test course description"
  end

end
