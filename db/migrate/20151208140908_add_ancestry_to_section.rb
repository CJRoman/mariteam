class AddAncestryToSection < ActiveRecord::Migration
  def change
    add_column :sections, :ancestry, :string
  end
end
