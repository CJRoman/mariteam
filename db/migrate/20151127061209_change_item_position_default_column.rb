class ChangeItemPositionDefaultColumn < ActiveRecord::Migration
  def change
    change_column_default :items, :position, 500
  end
end
