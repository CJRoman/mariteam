class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :question
      t.integer :question_type
      t.text :answers

      t.timestamps null: false
    end
  end
end
