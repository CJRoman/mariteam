class CreateCousesSectionsJoinTable < ActiveRecord::Migration
  def change
    create_table :courses_sections, id: false do |t|
      t.references :course
      t.references :section
    end
  end
end
