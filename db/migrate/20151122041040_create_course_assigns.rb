class CreateCourseAssigns < ActiveRecord::Migration
  def change
    create_table :course_assigns do |t|
      t.belongs_to :course, index: true, foreign_key: true
      t.belongs_to :user, index: true, foreign_key: true
      t.integer :ttl
      t.integer :max_tests_attempts
      t.date :started_at

      t.timestamps null: false
    end
  end
end
