class CreateItemsSectionsJoinTable < ActiveRecord::Migration
  def change
    create_table :items_sections, id: false do |t|
      t.references :item
      t.references :section
    end
  end
end
