class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.integer :position
      t.belongs_to :itemable, polymorphic: true

      t.timestamps null: false
    end
    add_index :items, [:itemable_id, :itemable_type]
  end
end
