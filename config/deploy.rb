lock '3.4.0'

set :application, 'Mariteam'
set :repo_url, 'git@bitbucket.org:CJRoman/mariteam.git'

set :deploy_to, '/home/railsuser/mariteam'
set :pty, true

set :linked_files, fetch(:linked_files, []).push('.env')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/ckeditor_assets')

set :keep_releases, 5