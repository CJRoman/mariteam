Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users
  devise_for :admin_users
  root to: "courses#index"

  namespace :admin do
    root to: "course_assigns#index"
    resources :admin_users, :users, :courses, :course_assigns, :sections
    resources :items, only: [:index, :destroy]
    resources :slides, :questions, except: [:index, :destroy]
  end

  resources :courses, only: [:index, :show] do
    resources :learning do
      post '' => 'learning#answer'
    end
  end
end
