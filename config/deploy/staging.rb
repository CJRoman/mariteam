server '127.0.0.1', user: 'railsuser', roles: %w{app web db}, ssh_options: { port: 2222 }
set :branch, ENV['BRANCH'] || 'development'