server '80.252.22.126', user: 'railsuser', roles: %w{app web db}
set :branch, ENV['BRANCH'] || 'master'
set :nginx_server_name, 'maritimetc.ru www.maritimetc.ru'