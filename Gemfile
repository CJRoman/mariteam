source 'https://rubygems.org'

# DATABASE
gem 'pg'

# SERVER
gem 'unicorn'

# FRAMEWORKS
gem 'rails', '4.2.4'
gem 'jbuilder', '~> 2.0'
gem 'dotenv-rails'
gem 'haml'
gem 'haml-rails'
gem 'bootstrap-sass'
gem 'font-awesome-sass'
gem 'devise'
gem 'cancancan'
gem 'russian'
gem 'gravatar_image_tag'
gem 'jquery-datatables-rails', '~> 3.3.0'
gem 'ajax-datatables-rails'
gem 'bootstrap_form'
gem 'select2_rails'
gem 'ckeditor', :git => 'https://github.com/galetahub/ckeditor.git'
gem 'paperclip'
gem 'ancestry'

# ASSETS
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'jquery-ui-rails'

# DEVELOPMENT TOOLS
group :development do
  gem 'pry-rails'
  gem 'quiet_assets'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'guard-livereload'
  gem "letter_opener"
end

# TESTING
group :development, :test do
  gem 'spring'
  gem 'rspec-rails'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'shoulda-matchers'
end

# PRODUCTION TOOLS
group :production, :staging do
  gem 'unicorn-worker-killer'
end

# DEPLOY
group :development do
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  gem 'capistrano-unicorn-nginx'
  gem 'sshkit-sudo'
end