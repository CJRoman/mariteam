class LearningController < ApplicationController
  before_action :authenticate_user!
  layout 'learning'

  def show
    @course = CourseAssign.find(params[:course_id])
    @sections = @course.course.sections
    @current_item = Item.find(params[:id])
    @items = @current_item.section.items.order(position: :asc).ids
    @counter_items = @current_item.section.items.order(position: :asc)
    @current_item_idx = @items.index(@current_item.id)
    @previous_item = (@current_item_idx-1 != -1) && @items[@current_item_idx-1]
    @next_item = (@current_item_idx+1 != @items.count) && @items[@current_item_idx+1]
  end

  def answer
    @item = Item.find(params[:learning_id])
    @answer = params[:answer]
    case @item.itemable.question_type
    when 0
      flash['correct'] = true if @item.itemable.answers.detect{|answer| answer['text'] == @answer}.try(:include?, "correct")
    when 1
      flash['correct'] = true if !@item.itemable.answers.find_all{|answer| answer['correct'].present? }.map{|answer| params[:answer].present? && params[:answer].include?(answer["text"])}.include?(false)
    when 2
      flash['correct'] = true if @item.itemable.answers["answer"].mb_chars.upcase == params[:answer].mb_chars.upcase
    when 3
      answer = params[:answer].split('|')
      correct_order = @item.itemable.answers.map{|a| a['query']}
      correct = true
      correct_order.each_with_index do |correct_answer, i|
        correct = false if correct_answer != answer[i]
      end
      flash['correct'] = true if correct
    when 4
      answer = params[:answer].split('|')
      correct_order = @item.itemable.answers.map{|a| a['right']}
      correct = true
      correct_order.each_with_index do |correct_answer, i|
        correct = false if correct_answer != answer[i]
      end
      flash['correct'] = true if correct
    end

    flash['answered'] = true
    redirect_to course_learning_path(params[:course_id], @item.id)
  end
end