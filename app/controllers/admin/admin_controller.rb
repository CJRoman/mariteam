class Admin::AdminController < ApplicationController
  layout "admin"
  before_filter { authenticate_admin_user! }
  before_filter { authorize! :manage, :all }
  before_filter :set_resource, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: (controller_name.classify+"Datatable").constantize.new(view_context) }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.json { render json: @resource }
    end
  end

  def new
    @resource = controller_name.classify.constantize.new
  end

  def edit    
  end

  def create
    @resource = controller_name.classify.constantize.new(permitted_params)
    if @resource.save
      respond_to do |format|
        format.html { redirect_to polymorphic_path([:admin, @resource.class]), notice: "#{@resource.model_name.human} создан" } 
        format.json { render json: @resource }
      end
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @resource = controller_name.classify.constantize.find(params[:id])
    if @resource.update_attributes(permitted_params)
      respond_to do |format|
        format.html { redirect_to polymorphic_path([:admin, @resource.class]), notice: "#{@resource.model_name.human} обновлен" } 
        format.json { render json: @resource }
      end
    else
      respond_to do |format|
        format.html { render :edit }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @resource.destroy
    respond_to do |format|
      format.html { redirect_to polymorphic_path([:admin, @resource.class]), notice: "#{@resource.model_name.human} удален" }
      format.json { head 200 }
    end
  end

  protected
  def set_resource
    @resource = controller_name.classify.constantize.find(params[:id])
  end
end