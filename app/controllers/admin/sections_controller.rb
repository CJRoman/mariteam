class Admin::SectionsController < Admin::AdminController

  def index
    if params[:select2].present?
      @resource = controller_name.classify.constantize.where("name ILIKE ?", '%' + params[:select2] + '%').offset(params[:page]).order(name: :asc)
    end
    if params[:tree].present?
      if params[:key]
        @resource = Section.find(params[:key]).children
      else
        @resource = Section.roots
      end
    end
    respond_to do |format|
      format.html
      format.json do
        if params[:select2].present?
          render json: @resource.map{|r| {id: r.id, text: r.name} }
        elsif params[:tree].present?
          render json: @resource.map{|r| {id: r.id, title: r.name, description: r.description, lazy: r.has_children?} }
        else
          render json: (controller_name.classify+"Datatable").constantize.new(view_context) 
        end
      end
    end
  end

  private
  def permitted_params
    params.require(:section).permit(:name, :description, :parent_id, :course_ids => [])
  end
  def set_resource
    @resource = Section.includes(:courses).find(params[:id])
  end
end