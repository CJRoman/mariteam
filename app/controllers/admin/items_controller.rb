class Admin::ItemsController < Admin::AdminController

  private
  def set_resource
    @resource = Item.find(params[:id])
  end
end