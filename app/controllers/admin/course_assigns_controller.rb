class Admin::CourseAssignsController < Admin::AdminController

  private
  def permitted_params
    params.require(:course_assign).permit(:user_id, :course_id, :ttl, :max_tests_attempts)
  end
end