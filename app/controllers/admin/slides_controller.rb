class Admin::SlidesController < Admin::AdminController

  def new
    @resource = Slide.new
    @resource.build_item
  end

  def create
    @resource = Slide.new(permitted_params)
    if @resource.save
      respond_to do |format|
        format.html { redirect_to polymorphic_path([:admin, Item]), notice: "Слайд создан" } 
        format.json { render json: @resource }
      end
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @resource = Slide.find(params[:id])
    if @resource.update_attributes(permitted_params)
      respond_to do |format|
        format.html { redirect_to polymorphic_path([:admin, Item]), notice: "Слайд обновлен" } 
        format.json { render json: @resource }
      end
    else
      respond_to do |format|
        format.html { render :edit }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def permitted_params
    params.require(:slide).permit(:name, :content, :item_attributes => [:name, :position, :section_id])
  end
  def set_resource
    @resource = Slide.find(params[:id])
  end
end