class Admin::AdminUsersController < Admin::AdminController

  def update
    if params[:admin_user][:password].blank? && params[:admin_user][:password_confirmation].blank?
      params[:admin_user].delete(:password)
      params[:admin_user].delete(:password_confirmation)
    end
    super
  end

  private
  def permitted_params
    params.require(:admin_user).permit(:email, :password, :password_confirmation)
  end
end