class Admin::UsersController < Admin::AdminController

  def create
    @resource = User.new(permitted_params)
    @resource.skip_confirmation!
    if @resource.save
      respond_to do |format|
        format.html { redirect_to admin_users_path, notice: "#{@resource.model_name.human} создан" } 
        format.json { render json: @resource }
      end
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
    super
  end

  private
  def permitted_params
    params.require(:user).permit(
      :first_name, 
      :father_name, 
      :family_name, 
      :birthday, 
      :telephone, 
      :personal_data_agreement, 
      :email, 
      :password, 
      :password_confirmation,
      :confirmed_at)
  end
end