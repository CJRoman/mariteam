class Admin::QuestionsController < Admin::AdminController

  def new
    @resource = Question.new
    @resource.build_item
  end

  def create
    @resource = Question.new(permitted_params)
    if @resource.save
      respond_to do |format|
        format.html { redirect_to polymorphic_path([:admin, Item]), notice: "Вопрос создан" } 
        format.json { render json: @resource }
      end
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @resource = Question.find(params[:id])
    if @resource.update_attributes(permitted_params)
      respond_to do |format|
        format.html { redirect_to polymorphic_path([:admin, Item]), notice: "Вопрос обновлен" } 
        format.json { render json: @resource }
      end
    else
      respond_to do |format|
        format.html { render :edit }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def permitted_params
    params.require(:question).permit(:question, :question_type, :answers => [:text, :correct, :answer, :query, :left, :right], :item_attributes => [:name, :position, :section_id])
  end
  def set_resource
    @resource = Question.find(params[:id])
  end
end