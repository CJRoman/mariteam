class Admin::CoursesController < Admin::AdminController

  private
  def permitted_params
    params.require(:course).permit(:name, :description)
  end
end