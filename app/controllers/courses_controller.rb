class CoursesController < ApplicationController
  before_action :authenticate_user!
  layout 'learning'

  def index
    @user_courses = CourseAssign
                    .eager_load(:course)
                    .where(user_id: current_user.id)
                    .order(created_at: :desc)
  end

  def show
    @user_course = CourseAssign
                    .find(params[:id])
    @sections = @user_course.course.sections
    respond_to do |format|
      format.html
      format.js { render layout: false}
    end
  end
end