class Section < ActiveRecord::Base
  validates :name, :course_ids, presence: true

  has_and_belongs_to_many :courses
  has_many :items

  has_ancestry
end
