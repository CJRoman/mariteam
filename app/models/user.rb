class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable, :timeoutable

  validates :first_name, :father_name, :family_name, :birthday, :telephone, :personal_data_agreement, presence: true

  def name
    "#{first_name} #{father_name} #{family_name}"
  end
end
