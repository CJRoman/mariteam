class Question < ActiveRecord::Base

  serialize :answers, JSON
  QUESTION_TYPES = ["Один ответ", "Несколько ответов", "Произвольный ввод", "Установка очередности", "Установка соответствия"]
  has_one :item, as: :itemable, dependent: :destroy
  accepts_nested_attributes_for :item, update_only: true
end
