class Item < ActiveRecord::Base
  belongs_to :itemable, polymorphic: true, dependent: :destroy
  belongs_to :section
  delegate :course, to: :section
  validates :name, :section_id, :position, presence: true
end
