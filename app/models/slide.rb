class Slide < ActiveRecord::Base
  validates :name, presence: true
  
  has_one :item, as: :itemable, dependent: :destroy
  accepts_nested_attributes_for :item, update_only: true
end
