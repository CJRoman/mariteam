class CourseAssign < ActiveRecord::Base
  belongs_to :course
  belongs_to :user

  validates :course, :user, :ttl, :max_tests_attempts, presence: true
end
