class AdminUserDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :render

  def sortable_columns
    @sortable_columns ||= %w(AdminUser.id AdminUser.email)
  end

  def searchable_columns
    @searchable_columns ||= %w(AdminUser.id AdminUser.email)
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        record.email,
        render(partial: "admin/partials/crud", locals: { record: record })
      ]
    end
  end

  def get_raw_records
    AdminUser.all
  end

end
