class CourseAssignDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :render, :truncate, :l

  def sortable_columns
    @sortable_columns ||= %w(course_assigns.id courses.name users.family_name course_assigns.ttl course_assigns.started_at course_assigns.max_tests_attempts)
  end

  def searchable_columns
    @searchable_columns ||= %w(course_assigns.id courses.name users.family_name course_assigns.ttl course_assigns.started_at course_assigns.max_tests_attempts)
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        record.course.name,
        record.user.name,
        record.ttl,
        record.started_at,
        record.max_tests_attempts,
        render(partial: "admin/partials/crud", locals: { record: record })
      ]
    end
  end

  def get_raw_records
    CourseAssign.eager_load(:course, :user).all
  end

end
