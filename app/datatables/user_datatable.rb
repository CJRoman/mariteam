class UserDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :render, :l

  def sortable_columns
    @sortable_columns ||= %w(User.id User.email User.first_name User.father_name User.family_name User.birthday User.telephone)
  end

  def searchable_columns
    @searchable_columns ||= %w(users.id users.email users.first_name users.father_name users.family_name users.birthday)
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        record.email,
        record.name,
        l(record.birthday, format: :long),
        record.telephone,
        render(partial: "admin/partials/crud", locals: { record: record })
      ]
    end
  end

  def get_raw_records
    User.all
  end

end
