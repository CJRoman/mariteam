class SectionDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :render, :truncate

  def sortable_columns
    @sortable_columns ||= %w(Section.id Section.name)
  end

  def searchable_columns
    @searchable_columns ||= %w(Section.id Section.name)
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        record.name,
        truncate(record.description, length: 100),
        render(partial: "admin/partials/crud", locals: { record: record })
      ]
    end
  end

  def get_raw_records
    Section.all
  end

end
