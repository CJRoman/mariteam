class ItemDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :render, :truncate, :t

  def sortable_columns
    @sortable_columns ||= %w(items.id itemable_type items.name items.section_id items.position)
  end

  def searchable_columns
    @searchable_columns ||= [
      "items.id",
      "items.itemable_type",
      "items.name",
      "section.name",
      "items.position"
    ]
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        t("activerecord.models.#{record.itemable_type.downcase}.other"),
        record.name,
        record.section.courses.present? ? record.section.courses.map{|c| "#{c.try(:name)} <i class='fa fa-angle-right'></i> #{record.section.name}".html_safe}.join("<br />") : record.section.name,
        record.position,
        render(partial: "admin/slides/crud", locals: { record: record })
      ]
    end
  end

  def get_raw_records
    Item.eager_load(:section).all
  end

end
