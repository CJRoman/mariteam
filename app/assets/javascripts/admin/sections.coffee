$ ->
  if $("#sectionsTable").length
    $("#sectionsTable").dataTable
      processing: true
      serverSide: true
      ajax: $("#sectionsTable").data('source')
      language:
        url: '/tools/dataTables.ru_lang.json'

  if $("#selectSectionTree").length
    $('#selectSectionTree').fancytree
      extensions: [
        'glyph'
        'table'
      ]
      glyph: 
        map:
          doc: "fa fa-user margin-10"
          docOpen: "fa fa-user margin-10"
          checkbox: "fa fa-square-o margin-10"
          checkboxSelected: "fa fa-check-square-o margin-10"
          checkboxUnknown: "fa fa-question margin-10"
          dragHelper: "fa fa-play margin-10"
          dropMarker: "fa fa-arrow-right margin-10"
          error: "fa fa-exclamation-triangle margin-10"
          expanderClosed: "fa fa-plus-square-o margin-10"
          expanderLazy: "fa fa-plus-square-o margin-10"
          expanderOpen: "fa fa-minus-square-o margin-10"
          folder: "fa fa-folder margin-10"
          folderOpen: "fa fa-folder-open margin-10"
          loading: "fa fa-refresh margin-10"
      source:
        url: "/admin/sections.json"
        method: "GET"
        data:
          tree: true
      autoScroll: true
      selectMode: 1
      table:
        nodeColumnIdx: 1
        indentation: 20  
      lazyLoad: (event, data) ->
        node = data.node
        data.result = {
          url: "/admin/sections.json"
          method: "GET"
          data: {key: node.data.id, tree: true}
        }
      renderColumns: (event, data) ->
        node = data.node
        $tdList = $(node.tr).find('>td')
        $tdList.eq(0).append(
          $("<input />")
            .attr("type", "radio")
            .attr("name", "section[parent_id]")
            .attr("value", node.data.id)
            .attr("checked", +$("#section_parent_id_hidden").val() == +node.data.id)
          " ",
          $("<span />").text(node.data.id)
        )
        $tdList.eq(1).append(
          $("<p />").html(node.data.name)
        )


  if $("#sectionsTree").length
    $('#sectionsTree').fancytree
      extensions: [
        'glyph'
        'table'
        'persist'
      ]
      glyph: 
        map:
          doc: "fa fa-folder margin-10"
          docOpen: "fa fa-folder-open margin-10"
          checkbox: "fa fa-square-o margin-10"
          checkboxSelected: "fa fa-check-square-o margin-10"
          checkboxUnknown: "fa fa-question margin-10"
          dragHelper: "fa fa-play margin-10"
          dropMarker: "fa fa-arrow-right margin-10"
          error: "fa fa-exclamation-triangle margin-10"
          expanderClosed: "fa fa-plus-square-o margin-10"
          expanderLazy: "fa fa-plus-square-o margin-10"
          expanderOpen: "fa fa-minus-square-o margin-10"
          folder: "fa fa-folder margin-10"
          folderOpen: "fa fa-folder-open margin-10"
          loading: "fa fa-refresh margin-10"
      source:
        url: "/admin/sections.json"
        method: "GET"
        data:
          tree: true
      autoScroll: true
      selectMode: 1
      table:
        nodeColumnIdx: 1
        indentation: 40
      persist:
        expandLazy: true
        store: "auto"
  
      lazyLoad: (event, data) ->
        node = data.node
        data.result = {
          url: "/admin/sections.json"
          method: "GET"
          data: 
            key: node.data.id
            tree: true
        }
      renderColumns: (event, data) ->
        node = data.node
        $tdList = $(node.tr).find('>td')
        $tdList.eq(0).text node.data.id
        $tdList.eq(2).text node.data.description
        $tdList.eq(3).append(
          $("<a />")
            .attr("href", "/admin/sections/#{node.data.id}")
            .attr("alt", "Просмотр")
            .attr("title", "Просмотр")
            .html("<i class='fa fa-eye'></i>")
            .addClass("btn btn-info btn-xs btn-flat"),
          " ",
          $("<a />")
            .attr("href", "/admin/sections/#{node.data.id}/edit")
            .attr("alt", "Редактирование")
            .attr("title", "Редактирование")
            .html("<i class='fa fa-pencil'></i>")
            .addClass("btn btn-warning btn-xs btn-flat"),
          " ",
          $("<a />")
            .attr("href", "/admin/sections/#{node.data.id}")
            .attr("alt", "Удаление")
            .attr("title", "Удаление")
            .attr("data-method", "delete")
            .attr("data-confirm", "Вы уверены?")
            .html("<i class='fa fa-trash'></i>")
            .addClass("btn btn-danger btn-xs btn-flat")
        )