$ ->
  if $("#coursesTable")
    $("#coursesTable").dataTable
      processing: true
      serverSide: true
      ajax: $("#coursesTable").data('source')
      language:
        url: '/tools/dataTables.ru_lang.json'