$ ->
  if $("#adminUsersTable")
    $("#adminUsersTable").dataTable
      processing: true
      serverSide: true
      ajax: $("#adminUsersTable").data('source')
      language:
        url: '/tools/dataTables.ru_lang.json'