$ ->
  if $("#itemsTable")
    $("#itemsTable").dataTable
      processing: true
      serverSide: true
      stateSave: true
      ajax: $("#itemsTable").data('source')
      language:
        url: '/tools/dataTables.ru_lang.json'

  if $("#slide_content").length
    CKEDITOR.inline('slide_content')

  if $("#question_question").length
    CKEDITOR.inline('question_question')

  if $("#slide_item_attributes_section_id").length
    $("#slide_item_attributes_section_id").select2
      theme: "bootstrap"
      ajax:
        url: '/admin/sections.json'
        dataType: 'json'
        delay: 250
        data: (params) ->
          {
            select2: params.term
            page: params.page
          }
        processResults: (data, params) ->
          params.page = params.page or 1
          {
            results: data
            pagination: more: params.page * 30 < data.length
          }
        cache: true

    $("#slide_item_attributes_section_id").on "select2:select", () ->
      $(".headings h3").text $(@).select2('data')[0].text
    $("#slide_name").on "input", () ->
      $(".headings h4").text $(@).val()

  
  if $("#question_item_attributes_section_id").length
    $("#question_item_attributes_section_id").select2
      theme: "bootstrap"
      ajax:
        url: '/admin/sections.json'
        dataType: 'json'
        delay: 250
        data: (params) ->
          {
            select2: params.term
            page: params.page
          }
        processResults: (data, params) ->
          params.page = params.page or 1
          {
            results: data
            pagination: more: params.page * 30 < data.length
          }
        cache: true
    $("#question_item_attributes_section_id").on "select2:select", () ->
      $(".headings h3").text $(@).select2('data')[0].text
      
  if $("#selectSlideItemSectionTree").length
    $('#selectSlideItemSectionTree').fancytree
      extensions: [
        'glyph'
        'table'
      ]
      glyph: 
        map:
          doc: "fa fa-folder margin-10"
          docOpen: "fa fa-folder-open margin-10"
          checkbox: "fa fa-square-o margin-10"
          checkboxSelected: "fa fa-check-square-o margin-10"
          checkboxUnknown: "fa fa-question margin-10"
          dragHelper: "fa fa-play margin-10"
          dropMarker: "fa fa-arrow-right margin-10"
          error: "fa fa-exclamation-triangle margin-10"
          expanderClosed: "fa fa-plus-square-o margin-10"
          expanderLazy: "fa fa-plus-square-o margin-10"
          expanderOpen: "fa fa-minus-square-o margin-10"
          folder: "fa fa-folder margin-10"
          folderOpen: "fa fa-folder-open margin-10"
          loading: "fa fa-refresh margin-10"
      source:
        url: "/admin/sections.json"
        method: "GET"
        data:
          tree: true
      autoScroll: true
      selectMode: 1
      table:
        nodeColumnIdx: 1
        indentation: 20  
      lazyLoad: (event, data) ->
        node = data.node
        data.result = {
          url: "/admin/sections.json"
          method: "GET"
          data: {key: node.data.id, tree: true}
        }
      renderColumns: (event, data) ->
        node = data.node
        $tdList = $(node.tr).find('>td')
        $tdList.eq(0).append(
          $("<input />")
            .attr("type", "radio")
            .attr("name", "slide[item_attributes][section_id]")
            .attr("value", node.data.id)
            .attr("checked", +$("#slide_item_sttributes_section_id_hidden").val() == +node.data.id)
          " ",
          $("<span />").text(node.data.id)
        )
        $tdList.eq(1).append(
          $("<p />").html(node.data.name)
        )

  if $("#selectQuestionItemSectionTree").length
    $('#selectQuestionItemSectionTree').fancytree
      extensions: [
        'glyph'
        'table'
      ]
      glyph: 
        map:
          doc: "fa fa-folder margin-10"
          docOpen: "fa fa-folder-open margin-10"
          checkbox: "fa fa-square-o margin-10"
          checkboxSelected: "fa fa-check-square-o margin-10"
          checkboxUnknown: "fa fa-question margin-10"
          dragHelper: "fa fa-play margin-10"
          dropMarker: "fa fa-arrow-right margin-10"
          error: "fa fa-exclamation-triangle margin-10"
          expanderClosed: "fa fa-plus-square-o margin-10"
          expanderLazy: "fa fa-plus-square-o margin-10"
          expanderOpen: "fa fa-minus-square-o margin-10"
          folder: "fa fa-folder margin-10"
          folderOpen: "fa fa-folder-open margin-10"
          loading: "fa fa-refresh margin-10"
      source:
        url: "/admin/sections.json"
        method: "GET"
        data:
          tree: true
      autoScroll: true
      selectMode: 1
      table:
        nodeColumnIdx: 1
        indentation: 20  
      lazyLoad: (event, data) ->
        node = data.node
        data.result = {
          url: "/admin/sections.json"
          method: "GET"
          data: {key: node.data.id, tree: true}
        }
      renderColumns: (event, data) ->
        node = data.node
        $tdList = $(node.tr).find('>td')
        $tdList.eq(0).append(
          $("<input />")
            .attr("type", "radio")
            .attr("name", "question[item_attributes][section_id]")
            .attr("value", node.data.id)
            .attr("checked", +$("#question_item_sttributes_section_id_hidden").val() == +node.data.id)
          " ",
          $("<span />").text(node.data.id)
        )
        $tdList.eq(1).append(
          $("<p />").html(node.data.name)
        )