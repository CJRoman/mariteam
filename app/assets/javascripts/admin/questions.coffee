$ ->
  if $("#answers")
    $("body").on 'click', '.removeRow', (e) ->
      e.preventDefault()
      $(this).closest("tr").remove() if $('.removeRow').length > 1
    $("body").on 'click', '.addRow', (e) ->
      e.preventDefault()
      question_type = $("#question_question_type").val()
      $("#addBtnRow").before(
        switch question_type
          when "0" then appendSingleAnswerRow()
          when "1" then appendMultipleAnswerRow()
          when "2" then appendTextAnswerRow()
          when "3" then appendQueryAnswerRow()
          when "4" then appendEqualAnswerRow()
      )
    $("#question_question_type").change ->
      $("#answers").empty()
      switch $(this).val()
        when "0" 
          $("#answers").append(
            $("<tr />").append(
              $("<td />")
                .attr("colspan", 3)
                .html("<p class='text-sm text-muted'>Введите варианты ответов и выберите <strong>ОДИН</strong> верный вариант. Во время теста ответы будут перемешаны автоматически.</p>")
            )
            appendSingleAnswerRow()
            $("<tr />").attr("id", "addBtnRow").append(
              $("<td />")
                .attr("colspan", 3)
                .html("<a href='#' class='btn btn-xs btn-info addRow'>Добавить строку</a>")
            )
          )

        when "1" 
          $("#answers").append(
            $("<tr />").append(
              $("<td />")
                .attr("colspan", 3)
                .html("<p class='text-sm text-muted'>Введите варианты ответов и выберите <strong>НЕСКОЛЬКО</strong> верных вариантов. Во время теста ответы будут перемешаны автоматически.</p>")
            )
            appendMultipleAnswerRow()
            $("<tr />").attr("id", "addBtnRow").append(
              $("<td />")
                .attr("colspan", 3)
                .html("<a href='#' class='btn btn-xs btn-info addRow'>Добавить строку</a>")
            )
          )
        when "2" 
          $("#answers").append(
            $("<tr />").append(
              $("<td />")
                .attr("colspan", 1)
                .html("<p class='text-sm text-muted'>Введите <strong>ВЕРНЫЙ ВАРИАНТ ОТВЕТА</strong>. Регистр букв не имеет значения.<br />Постарайтесь описать ответ одним словом или цифрой. В противном случае, испытуемый рискует ввести неверный с точки зрения системы ответ.</p>")
            )
            appendTextAnswerRow()
          )
        when "3" 
          $("#answers").append(
            $("<tr />").append(
              $("<td />")
                .attr("colspan", 2)
                .html("<p class='text-sm text-muted'>Введите варианты ответов <strong>В ПРАВИЛЬНОМ ПОРЯДКЕ</strong>. Во время теста ответы будут перемешаны автоматически.</p>")
            )
            appendQueryAnswerRow()
            $("<tr />").attr("id", "addBtnRow").append(
              $("<td />")
                .attr("colspan", 2)
                .html("<a href='#' class='btn btn-xs btn-info addRow'>Добавить строку</a>")
            )
          )
        when "4" 
          $("#answers").append(
            $("<tr />").append(
              $("<td />")
                .attr("colspan", 3)
                .html("<p class='text-sm text-muted'>Введите варианты ответов <strong>В ПРАВИЛЬНОМ СООТВЕТСТВИИ ДРУГ ДРУГУ</strong>. Во время теста ответы будут перемешаны автоматически.</p>")
            )
            appendEqualAnswerRow()
            $("<tr />").attr("id", "addBtnRow").append(
              $("<td />")
                .attr("colspan", 3)
                .html("<a href='#' class='btn btn-xs btn-info addRow'>Добавить строку</a>")
            )
          )

appendSingleAnswerRow = ->
  $("<tr />").append(
    $("<td />").append(
      $("<input />")
        .attr("type", "text")
        .attr("name", "question[answers][][text]")
        .attr("placeholder", "Введите вариант ответа")
        .addClass("form-control")
    )
    $("<td />").append(
      $("<input />")
        .attr("type", "radio")
        .attr("name", "question[answers][][correct]"),
      $("<span />").text(" Это верный ответ")
    )
    $("<td />").append(
      $("<a />")
        .addClass("btn btn-danger btn-xs removeRow")
        .text("Удалить ответ")
    )
  )

appendMultipleAnswerRow = ->
  $("<tr />").append(
    $("<td />").append(
      $("<input />")
        .attr("type", "text")
        .attr("name", "question[answers][][text]")
        .attr("placeholder", "Введите вариант ответа")
        .addClass("form-control")
    )
    $("<td />").append(
      $("<input />")
        .attr("type", "checkbox")
        .attr("name", "question[answers][][correct]"),
      $("<span />").text(" Это верный ответ")
    )
    $("<td />").append(
      $("<a />")
        .addClass("btn btn-danger btn-xs removeRow")
        .text("Удалить ответ")
    )
  )

appendTextAnswerRow = ->
  $("<tr />").append(
    $("<td />").append(
      $("<input />")
        .attr("type", "text")
        .attr("name", "question[answers][answer]")
        .attr("placeholder", "Введите правильный ответ")
        .addClass("form-control")
    )
  )

appendQueryAnswerRow = ->
  $("<tr />").append(
    $("<td />").append(
      $("<input />")
        .attr("type", "text")
        .attr("name", "question[answers][][query]")
        .attr("placeholder", "Введите ответ")
        .addClass("form-control")
    )
    $("<td />").append(
      $("<a />")
        .addClass("btn btn-danger btn-xs removeRow")
        .text("Удалить ответ")
    )
  )

appendEqualAnswerRow = ->
  $("<tr />").append(
    $("<td />").append(
      $("<input />")
        .attr("type", "text")
        .attr("name", "question[answers][][left]")
        .attr("placeholder", "Введите левую сторону соответствия")
        .addClass("form-control")
    )
    $("<td />").append(
      $("<input />")
        .attr("type", "text")
        .attr("name", "question[answers][][right]")
        .attr("placeholder", "Введите правую сторону соответствия")
        .addClass("form-control")
    )
    $("<td />").append(
      $("<a />")
        .addClass("btn btn-danger btn-xs removeRow")
        .text("Удалить ответ")
    )
  )