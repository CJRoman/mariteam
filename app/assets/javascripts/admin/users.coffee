$ ->
  if $("#usersTable")
    $("#usersTable").dataTable
      processing: true
      serverSide: true
      ajax: $("#usersTable").data('source')
      language:
        url: '/tools/dataTables.ru_lang.json'