$ ->
  if $("#courseAssignsTable")
    $("#courseAssignsTable").dataTable
      processing: true
      serverSide: true
      ajax: $("#courseAssignsTable").data('source')
      language:
        url: '/tools/dataTables.ru_lang.json'