//= require jquery_ujs
//= require jquery-ui
//= require jquery.colorbox-min
//= require jquery.cookie
//= require AdminLTE.min
//= require jquery.fancytree-all.min
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require select2
//= require ckeditor/init
//= require ckeditor/config
//= require ckeditor/mediaembed/plugin
//= require ckeditor/sourcedialog/plugin
//= require ckeditor/tableresize/plugin
//= require ckeditor/sourcedialog/lang/ru
//= require ckeditor/sourcedialog/dialogs/sourcedialog

//= require admin/admin_users
//= require admin/users
//= require admin/courses
//= require admin/sections
//= require admin/course_assigns
//= require admin/items
//= require admin/questions

$ ->
  $(".colorbox").colorbox()