CKEDITOR.editorConfig = ( config ) ->
  config.allowedContent = true
  config.filebrowserBrowseUrl = "/ckeditor/attachment_files"
  config.filebrowserFlashBrowseUrl = "/ckeditor/attachment_files"
  config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files"
  config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures"
  config.filebrowserImageBrowseUrl = "/ckeditor/pictures"
  config.filebrowserImageUploadUrl = "/ckeditor/pictures"
  config.filebrowserUploadUrl = "/ckeditor/attachment_files"

  config.toolbarGroups = [
    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
    { name: 'forms', groups: [ 'forms' ] },
    { name: 'links', groups: [ 'links' ] },
    { name: 'mediaembed', groups: [ 'mediaembed' ]},
    '/',
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
    { name: 'insert', groups: [ 'insert' ] },
    '/',
    { name: 'styles', groups: [ 'styles' ] },
    { name: 'colors', groups: [ 'colors' ] },
    { name: 'tools', groups: [ 'tools' ] },
    { name: 'others', groups: [ 'others' ] },
    { name: 'about', groups: [ 'about' ] }
  ]


  config.removeButtons = 'Styles,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Save,NewPage,Preview,Print,Templates,Paste,Find,Replace,SelectAll,Scayt,BidiLtr,BidiRtl,Language,Smiley,PageBreak,About,Maximize';
  config.extraPlugins = 'mediaembed,sourcedialog,tableresize'

  config.filebrowserParams = ->
    csrf_token
    csrf_param 
    meta
    metas = document.getElementsByTagName('meta')
    params = new Object()

    for i in [0..metas.length]
      meta = metas[i]

      switch meta.name
        when "csrf-token" then csrf_token = meta.content
        when "csrf-param" then csrf_param = meta.content
        else continue

    if csrf_param != undefined && csrf_token != undefined
      params[csrf_param] = csrf_token

    params
  
  config.addQueryString = ( url, params ) ->
    queryString = []

    if !params 
      return url
    else
      for i in params
        queryString.push( i + "=" + encodeURIComponent( params[ i ] ) )

    url + ( ( url.indexOf( "?" ) != -1 ) ? "&" : "?" ) + queryString.join( "&" )

  CKEDITOR.on( 'dialogDefinition', ( ev ) ->
    dialogName = ev.data.name
    dialogDefinition = ev.data.definition
    content
    upload

    if CKEDITOR.tools.indexOf(['link', 'image', 'attachment', 'flash'], dialogName) > -1
      content = (dialogDefinition.getContents('Upload') || dialogDefinition.getContents('upload'))
      upload = (content == null ? null : content.get('upload'))

      if upload && upload.filebrowser && upload.filebrowser['params'] == undefined
        upload.filebrowser['params'] = config.filebrowserParams()
        upload.action = config.addQueryString(upload.action, upload.filebrowser['params'])
      
    
  )