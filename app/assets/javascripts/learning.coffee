$ ->
  $("#query_answers").sortable
    stop: () ->
      $("#answer").val( $(@).sortable('toArray').join('|') )

  $("#equality_answers").sortable
    stop: () ->
      $("#answer").val( $(@).sortable('toArray').join('|') )